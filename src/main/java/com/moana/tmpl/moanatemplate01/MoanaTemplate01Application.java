package com.moana.tmpl.moanatemplate01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MoanaTemplate01Application {

    public static void main(String[] args) {
        SpringApplication.run(MoanaTemplate01Application.class, args);
    }

}
